FROM kwonghung/docker-sdkman
LABEL maintainer="kwonghung.yip@gmail.com"

ARG SDK_JDK_VER

SHELL ["/bin/bash","-c"]

RUN source ${SDKMAN_DIR}/bin/sdkman-init.sh; \
    sdk install java ${SDK_JDK_VER}; \
    sdk list java; \
    sdk offline; \
    sdk flush archives; \
    sdk flush temp;

ENV JAVA_HOME=${SDKMAN_DIR}/candidates/java/${SDK_JDK_VER}
ENV PATH=${JAVA_HOME}/bin:${PATH}

RUN java -version

CMD ["bash"]

